package com.borombo.nasa

import com.borombo.nasa.api.model.NearEarthObject
import com.borombo.nasa.utils.ListConverterUtils
import org.junit.Before
import org.junit.Test

class ListConverterUtilsTest {

    private lateinit var oneElementMap : Map<String, Array<NearEarthObject>>
    private lateinit var oneHeaderTwoChildMap : Map<String, Array<NearEarthObject>>

    @Before
    fun setup(){
        oneElementMap = mapOf("Element 1" to arrayOf(NearEarthObject(
            "id",
            "name",
            "url"
        )))

        oneHeaderTwoChildMap = mapOf("Element 1" to arrayOf(
            NearEarthObject("id", "name", "url"),
            NearEarthObject("id2", "name2", "url2")
        ))
    }

    @Test
    fun convertEmptyMapTest(){
        val res = ListConverterUtils.mapToList(emptyMap())
        assert(res.isEmpty())
    }

    @Test
    fun convertOneHeaderOneChildTest(){
        val res = ListConverterUtils.mapToList(oneElementMap)
        assert(res.isNotEmpty())
        assert(res.size == 2)
    }

    @Test
    fun convertOneHeaderTwoChildTest(){
        val res = ListConverterUtils.mapToList(oneHeaderTwoChildMap)
        assert(res.isNotEmpty())
        assert(res.size == 3)
    }

    @Test
    fun convertOneHeaderType(){
        val res = ListConverterUtils.mapToList(oneElementMap)
        assert(res.isNotEmpty())
        assert(res[0] is String)
    }

    @Test
    fun convertOneChildType(){
        val res = ListConverterUtils.mapToList(oneElementMap)
        assert(res.isNotEmpty())
        assert(res[1] is NearEarthObject)
    }




}