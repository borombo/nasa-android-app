package com.borombo.nasa.api

import com.borombo.nasa.utils.NASA_API_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NasaAPI {

    private var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(NASA_API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    // Services
    var neoFeedService: NeoFeedService

    init {
        neoFeedService = retrofit.create(NeoFeedService::class.java)
    }

}