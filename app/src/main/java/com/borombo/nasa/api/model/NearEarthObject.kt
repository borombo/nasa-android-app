package com.borombo.nasa.api.model

data class NearEarthObject(val id: String,
                           val name: String,
                           val nasa_jpl_url: String)