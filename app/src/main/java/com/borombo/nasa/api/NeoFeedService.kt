package com.borombo.nasa.api

import com.borombo.nasa.api.model.NeoFeedResponse
import com.borombo.nasa.utils.KEY_NASA_API
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NeoFeedService {

    @GET("neo/rest/v1/feed")
    fun getFeed(@Query(KEY_NASA_API) apiKey: String): Call<NeoFeedResponse>

}