package com.borombo.nasa.api.model

data class NeoFeedResponse(val element_count: Int,
                           val near_earth_objects : Map<String, Array<NearEarthObject>>)