package com.borombo.nasa.repository

import com.borombo.nasa.BuildConfig
import com.borombo.nasa.api.NasaAPI
import com.borombo.nasa.api.model.NeoFeedResponse
import retrofit2.Callback


class NeoFeedRepository {

    /**
     * Get the data from the api and use the view model callback
     */
    fun getFeedFromSource(callback: Callback<NeoFeedResponse>){
        NasaAPI.neoFeedService.getFeed(BuildConfig.apiKey).enqueue(callback)
    }
}