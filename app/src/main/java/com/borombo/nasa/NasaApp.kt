package com.borombo.nasa

import android.app.Application
import timber.log.Timber

class NasaApp : Application(){

    override fun onCreate() {
        super.onCreate()
        setup()
    }

    /**
     * Setup everything needed for the app
     */
    private fun setup(){
        Timber.plant(Timber.DebugTree())
    }
}