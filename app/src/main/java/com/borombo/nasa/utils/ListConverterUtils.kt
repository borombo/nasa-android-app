package com.borombo.nasa.utils

import com.borombo.nasa.api.model.NearEarthObject

object ListConverterUtils {

    /**
     * Convert a map to a list of Any object to handle it in the recycler view
     */
    fun mapToList(map: Map<String, Array<NearEarthObject>>): ArrayList<Any>{
        val list = arrayListOf<Any>()

        for (mapEntry in map){
            list.add(mapEntry.key)
            for(nearEarthObject in mapEntry.value){
                list.add(nearEarthObject)
            }
        }
        return list
    }

}