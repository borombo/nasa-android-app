package com.borombo.nasa.ui

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.borombo.nasa.R
import com.borombo.nasa.api.model.NearEarthObject
import com.bumptech.glide.Glide

class NearEarthObjectViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    // Get a reference to the object, if needed
    private lateinit var nearEarthObject : NearEarthObject

    /**
     * Bind the object with the view
     */
    fun bindNearEarthObject(nearEarthObject: NearEarthObject){
        this.nearEarthObject = nearEarthObject
        setupImage(itemView.findViewById<ImageView>(R.id.neoObjectImage))
        setupText()
    }

    /**
     * Setup the image of the view
     */
    private fun setupImage(imageView: ImageView){
        Glide.with(itemView)
            .load(nearEarthObject.nasa_jpl_url)
            .into(imageView)
    }

    /**
     * Setup the text of the view
     */
    private fun setupText(){
        itemView.findViewById<TextView>(R.id.neoObjectName).text = nearEarthObject.name
        itemView.findViewById<TextView>(R.id.neoObjectId).text = nearEarthObject.id
    }
}