package com.borombo.nasa.ui

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.borombo.nasa.R

class NearEarthObjectHeaderViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

    fun bindHeader(text: String){
        itemView.findViewById<TextView>(R.id.headerText).text = text
    }
}