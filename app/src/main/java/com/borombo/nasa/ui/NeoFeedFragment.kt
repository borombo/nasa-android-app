package com.borombo.nasa.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

import com.borombo.nasa.R
import com.borombo.nasa.utils.ListConverterUtils
import kotlinx.android.synthetic.main.neo_feed_fragment.*

class NeoFeedFragment : Fragment() {

    private lateinit var viewModel: NeoFeedViewModel
    private lateinit var adapter: NearEarthObjectAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.neo_feed_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NeoFeedViewModel::class.java)
        setupRecyclerView()
        setupViewModel()
    }

    /**
     * Setup all the listener needed to the viewModel
     */
    private fun setupViewModel(){
        viewModel.getFeedObjects().observe(this, Observer { feedObjects ->
            // Update the number of object found
            numberObjectText.text = getString(R.string.neo_object_number, feedObjects?.element_count.toString())
            // Update the list with the new data
            adapter.setData(ListConverterUtils.mapToList(feedObjects.near_earth_objects))
        })
    }

    /**
     * Setup the recycler view and the adapter
     */
    private fun setupRecyclerView(){
        adapter = NearEarthObjectAdapter()
        neoFeedRecyclerView.layoutManager = LinearLayoutManager(context)
        neoFeedRecyclerView.adapter = adapter
    }

}
