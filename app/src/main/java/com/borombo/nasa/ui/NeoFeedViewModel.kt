package com.borombo.nasa.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.borombo.nasa.api.model.NeoFeedResponse
import com.borombo.nasa.repository.NeoFeedRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class NeoFeedViewModel : ViewModel() {

    // Data in a LiveData
    private lateinit var feedObjects : MutableLiveData<NeoFeedResponse>

    // Repository
    private var neoFeedRepository: NeoFeedRepository = NeoFeedRepository()

    /**
     * Send a LiveData of the feed to the view model
     */
    fun getFeedObjects(): LiveData<NeoFeedResponse> {
        if (!::feedObjects.isInitialized) {
            feedObjects = MutableLiveData()
            loadFeed()
        }
        return feedObjects
    }

    /**
     * Call the repository to get the data
     * Could be could to refresh the data
     */
    private fun loadFeed(){
        neoFeedRepository.getFeedFromSource(object : Callback<NeoFeedResponse> {
            override fun onFailure(call: Call<NeoFeedResponse>, t: Throwable) {
                Timber.d("Request fail: ${t.message}")
            }

            override fun onResponse(call: Call<NeoFeedResponse>, response: Response<NeoFeedResponse>) {
                if (response.isSuccessful){
                    Timber.d("Response successful")

                    feedObjects.value = response.body()
                }else{
                    Timber.d("Response not successful")
                }
            }
        })
    }

}
