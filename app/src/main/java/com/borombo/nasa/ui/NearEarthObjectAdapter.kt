package com.borombo.nasa.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.borombo.nasa.R
import com.borombo.nasa.api.model.NearEarthObject

class NearEarthObjectAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val HEADER_TYPE = 1
        const val ROW_TYPE = 2
    }

    private var listOfNearEarthObject : ArrayList<Any> = arrayListOf()

    /**
     * Set the data for the list
     * @param data the list of object
     */
    fun setData(data: ArrayList<Any>){
        listOfNearEarthObject = data
        notifyDataSetChanged()
    }

    /**
     * Create the viewHolder depending on the type of the object
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val contentView: View?
        return when(viewType){
            HEADER_TYPE -> {
                contentView = LayoutInflater.from(parent.context).inflate(R.layout.row_near_earth_object_header, parent, false)
                NearEarthObjectHeaderViewHolder(contentView)
            }
            else -> {
                contentView =  LayoutInflater.from(parent.context).inflate(R.layout.row_near_earth_object, parent, false)
                NearEarthObjectViewHolder(contentView)
            }
        }
    }

    override fun getItemCount(): Int { return listOfNearEarthObject.size }

    /**
     * Bind the view with the right view holder depending on this type
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = listOfNearEarthObject[position]
        if (data is String)
            (holder as NearEarthObjectHeaderViewHolder).bindHeader(data)
        else
            (holder as NearEarthObjectViewHolder).bindNearEarthObject(data as NearEarthObject)
    }

    /**
     * Select the itemViewType depending on type of the object at the position in the list
     */
    override fun getItemViewType(position: Int): Int {
        var type = -1
        if (listOfNearEarthObject[position] is String){
            type = HEADER_TYPE
        }else if (listOfNearEarthObject[position] is NearEarthObject){
            type = ROW_TYPE
        }
        return type
    }
}